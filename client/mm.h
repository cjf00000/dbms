#ifndef __MM_H
#define __MM_H

#include <vector>
#include <stack>
#include <list>
#include <cstdio>
using namespace std;

class MM
{
public:
	static const int PageSize = 4096;
	static const int NumQueues = 1;
	static const int LevelUpThreshold = 3;
	static const int FileSize = 1024 * 1024 * 1024;

	/**
		@note	capacity should be multiple of NumQueues * PageSize	
	*/
	MM(const char *pageFileName, long long capacity);

	int newPage();

	/**
		@note	The result would only guaranteed to be valid before next
			getPage() is called. If you want a longer valid period, 
			copy the content to your own memory.
	*/
	void* getPage(int ID);

	/**
		@note	Don't forget to call this if you modified the content.
			Otherwise the modification would be lost
	*/
	void setPageDirty(int ID);

	static void test();

	int sb;

private:
	char *pageFileName;
	int N, C;

	/**
		Multiqueue algorithm:

		accessCount is the main creteria used to level up the pages:
		level up when LevelUpThreshold reached
		OR the upper level is not full
	*/
	// LRU, in cpn
	vector<list<int> >LRU;
	vector<int> LRUSize;
	// Access by cpn:	LRUEntry[cpn]
	vector<list<int>::iterator> LRUEntry;
	// Access by cpn:	LRULevel[cpn]
	vector<int> LRULevel;	
	vector<int> accessCount;
	int lastPage;
	int levelCapacity;

	// PPN --> CPN(page number in cache), CPN=-1 is miss
	vector<int> cpnLUT;

	// Data cache, capacity = rowCacheSize
	char *cache;

	// cpnToPPN[cpn] and dirty[cpn] is useful when writeback
	vector<int> cpnToPPN; 
	vector<bool> dirty;

	// Free page table (in cache page number)
	stack<int> freePage;

	// Real pageFile, located in disk
	vector<FILE*> pageFile;


	// ---- Methods:

	/**
		Swap ppn into cache.
		@Return		Begin of cache page
	*/
	void* swap_page(int ppn);

	/**
		Get a free page from *CACHE*. If fail, evict a page.
		@Return		CPN of that page
	*/
	int getFreePage();

	/**
		Evict cpn
	*/
	void evict(int cpn);

	/** 	
	  	Write cpn back
	*/
	void writeBack(int cpn);

	/**
		Load from pageFile[ppn] to cache[cpn]
	*/
	void loadPage(int cpn, int ppn);

	static void showQueue(MM *mm);
};

#endif
