#ifndef __TABLE_H
#define __TABLE_H

#include <queue>
#include <stack>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <map>
using namespace std;

/**
  	An record is a array of data.
*/
typedef int* Record;

/**
  	Usage
	====
	1.	Create a MM
	2.	Use the MM to construct each table.

	Storage and Access
	====
	All the fields are integers.
	The table can be accessed with (r) for a record, or (r, c) for a integer.

	Each page stores (PageSize / 4C) records.


  	Cache
	====

	The cache is fully associated.

	Physical page number:	Number of pages offset in disk.
	Cache page number:	Number of pages offset in cache. (if a cache hit)
*/

class MM;

class Table {
private:
#ifdef DEBUG_MODE
public:
#endif
	// LPN(page number in this table) --> PPN(page number in disk)
	vector<int> ppnLUT;

	// Dimension of the table
	int R, C;

	int recordSize, recordPerPage;

public:
	Table(MM* memory, int C);

	/**
		@note	The returned record is read-only. Write on the record
			will take no effect. Use <push> to modify a record.
			(Note we only have INSERT operations.)
	*/
	Record itemAt(int r);

	int itemAt(int r, int c);

	int getNumberOfRow();

	void push(Record record);

	~Table();

	static void test();

private:

	/**
		Returns a tuple (Physical Page Number, offset in page)
	*/
	pair<int, int> locate(int r);

	MM* memory;

	int* lastPageBegin;
	int lastPPN;
};

extern vector<Table*> db;

#endif
