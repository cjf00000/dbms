#ifndef __COMPOUND_RES
#define __COMPOUND_RES

#include <algorithm>
#include <cstdio>
#include <vector>
#include "structs.h"
using namespace std;

class CompoundIndex;
class CompoundRestrictionItem;
class MM;

class CompoundRestriction
{
public:
	CompoundRestriction(int numColumn);
	void trainItem(vector<ColumnRestriction> &restriction);
	CompoundRestrictionItem& optimumItem(vector<ColumnRestriction> &restriction);

	static void test();

	static CompoundRestrictionItem toItem(vector<ColumnRestriction> &restriction);
	vector<CompoundRestrictionItem> combs;
};

class CompoundRestrictionItem
{
public:
	static MM* memory;

	CompoundRestrictionItem(vector<int> e, int ie);

	bool covered(int columnID)
	{
		for (int i=0; i<e.size(); ++i)
			if (e[i]==columnID)
				return true;

		return false;
	}

	int size() { return e.size(); }

	// Use this index
	CompoundIndex* index;

	// ie is better than e
	// equals, in column ID, last is iequal
	vector<int> e;

	void print()
	{
		for (int i=0; i<e.size(); ++i)
			printf("%d\t", e[i]);
		printf("\n");
	}
};


#endif 
