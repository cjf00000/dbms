#ifndef __SUB_INDEX
#define __SUB_INDEX

#include <vector>
#include "index.h"
using namespace std;

class SubIndex : public Index
{
public:
	SubIndex();

	/// Basic operations
	virtual void push(int *key, int value) = 0;

	virtual void rangeQuery(int *upper, int *lower,
			vector<int>& result) = 0;

	virtual void getKth(int k, int *ret) = 0;
	virtual void begin() = 0;
	virtual void next(int *ret) = 0;

	virtual ~SubIndex();

	/// 0 = Blocklist
	/// 1 = DiskArray
	int Type;

	/// Number of items
	int N;
};


#endif
