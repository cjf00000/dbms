#ifndef __STRING_LUT
#define __STRING_LUT

#include <vector>
#include <string>
using namespace std;

class Table;
class MM;
typedef int* Record;

/**
  	THIS INSTRUCTION MAY BE DEPRECATED

	String Look Up Table
	====

	The class is a look up table for strings.
	Strings are unique, namely, each string will be stored only once.
	
	Each column should use its dedicated look up table.

	Usage
	====

	1.	Initialize

	StringLUT should be initialized *AFTER ALL TABLES INITIALIZED*, *BEFORE Table::setRowCacheSize* called.

	For each string column:
		1.	Construct a String LUT
		2.	Set string length
		3.	Set number of hash table entries
			* MUST * be exponetial of 2
	
	2.	Insert new string
		push(stringToBeInserted);
		@Return	the ID of stringToBeInserted

	3.	Look up a string's id
		getID(string)

	4.	Search for string #id
		getString(ID)			OR
		getString(vector<ID>)
*/
class StringLUT
{
public:
	/**
	  	Construct a StringLUT
	*/
	StringLUT(MM *memory, vector<int> length);

	void push(vector<char*> &str);

	//	@note	length(str) must be multiple of 4
	void getString(char* str, int r, int c);

	static int hash(char* str);

	~StringLUT();

	static void test();

private:

	/// Length of the string
	vector<int> sBegin;
	vector<int> sEnd;
	int sLength;

	/// Length of the record
	vector<int> rBegin;
	vector<int> rEnd;
	int rLength;

	char* myBuffer;

	void toRecord(char* str, int sl, Record record);
	void toString(char* str, Record record, int Begin, int End);

	/**
		Real string stores in table, as Records.

		We to this to share the buffer implementation of Table.
	*/
	Table *table;
};

#endif
