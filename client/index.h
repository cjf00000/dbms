#ifndef __INDEX
#define __INDEX

#include <vector>
#include <cstdio>
using namespace std;

class CompareArray
{
public:
	CompareArray(int items) { this->items = items; }
	bool operator() (int* a, int *b)
	{
		for (int i=0; i<items; ++i)
		{
			if (a[i]!=b[i])
				return a[i]<b[i];
		}
		return false;
	}

private:
	int items;
};

class Index
{
public:
	Index();

	/// Basic operations
	virtual void push(int *key, int value) = 0;

	virtual void rangeQuery(int *upper, int *lower,
			vector<int>& result) = 0;

	virtual int usage() = 0;

	virtual ~Index();

	virtual void test();
};


#endif
