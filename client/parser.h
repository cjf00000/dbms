#ifndef __PARSER
#define __PARSER

#include "util.h"

extern map<string, int > tableToTableID;
extern map<string, int > columnToColumnID;
extern map<string, int > columnToTableID;

void parseInsert(vector<string> &tokens)
{
	vector<string> rows;
	for (int i=5; i<tokens.size(); i+=4)
	{
		string& currentRow = tokens[i];
		rows.push_back(currentRow);
	}
	load(tokens[2], rows);
}

bool parseSelect(vector<string> &tokens, 
		vector<GlobalColumnID>& selectedColumns, 
		vector<int>& fromTableID, 
		vector<ColumnRestriction>& restriction,
		vector<pair<GlobalColumnID, GlobalColumnID> >& equalPairs,
		vector<StringRestriction>& stringRestriction
		)
{
	selectedColumns.clear();
	//columnIsString.clear();
	fromTableID.clear();
	restriction.clear();
	equalPairs.clear();

	for (int i=0; i<stringRestriction.size(); ++i)
		delete stringRestriction[i].value;
	stringRestriction.clear();

	int pFrom, pWhere, pSelect=0;
	for (pFrom=0; pFrom<tokens.size(); ++pFrom)
		if (tokens[pFrom]=="FROM")
			break;

	for (pWhere=0; pWhere<tokens.size(); ++pWhere)
		if (tokens[pWhere]=="WHERE")
			break;

	for (int i=pSelect+1; i<pFrom; i+=2) 			// Parse selected columns
		selectedColumns.push_back( GlobalColumnID( columnToTableID[ tokens[i] ],
							   columnToColumnID[ tokens[i] ] ) );
	
	for (int i=pFrom+1; i<pWhere; i+=2)			// Parse from tables
		fromTableID.push_back( tableToTableID[ tokens[i] ] );

	bool emptyResult = 0;

	for (int i=pWhere+1; i<tokens.size(); i+=4 )		// Parse restrictions
	{
		GlobalColumnID ID1( columnToTableID[ tokens[i] ], columnToColumnID[ tokens[i] ] );
		pair<int, int> range;

		bool isJoin = ( !isNumber( tokens[i+2] ) && !isStringConst( tokens[i+2] ) );

		if (isJoin)
		{
			GlobalColumnID ID2( columnToTableID[ tokens[i+2] ],
						columnToColumnID[ tokens[i+2] ] );
			equalPairs.push_back( make_pair(ID1, ID2) );
		}
		else
		{
			int p=0;
			for (p=0; p<restriction.size(); ++p)
				if ( restriction[p].ID.tableID == ID1.tableID 
				 &&  restriction[p].ID.columnID == ID1.columnID  )
					break;

			if (p==restriction.size())
				restriction.push_back( ColumnRestriction( ID1, make_pair(-INF-1, INF) ) );

			if (isNumber(tokens[i+2]))	
			{
				// Number: compare with real number
				restriction[p].range = setJoin(restriction[p].range, 
							makeRange( tokens[i+1], tokens[i+2] ));
			}
			else
			{
				// Hash: comapre with hash value
				int myLen = tokens[i+2].size()-2;
				char *value = new char[myLen+1];
				memcpy(value, tokens[i+2].c_str()+1, myLen+1);
				value[myLen] = '\0';

				// Don't join the range, but replace the former one
				// Check the replaced condition later (during next).
				restriction[p].range.first = 
				restriction[p].range.second = 
				StringLUT::hash( value );

				stringRestriction.push_back(
						StringRestriction( ID1, value ) );
			}

			if (restriction[p].range.first > restriction[p].range.second)
				emptyResult = 1;
		}
	}

	return emptyResult;
}

#endif
