﻿#include "algorithm.h"
#include <algorithm>
#include "compoundindex.h"
#include "mm.h"
#include <cassert>
#include <cstdio>
#include <iostream>
#include "table.h"
using namespace std;

struct BucketPair
{
	int value;
	int pos;
};

#define BUCNUM 65536

vector<int>* Algorithm::select_BruteForce( int tableID, 
				vector<int> &columnID, 
				vector<int> &left, 
				vector<int> &right, 
				vector<int> *from )
{
	bool flag = true;
	vector<int> *result = new vector<int>();

	if(from == NULL)		//原始表类
	{
		for(int i = 0;i<db[tableID]->getNumberOfRow();i++)
		{
			flag = true;
			for(int j = 0;j<columnID.size();j++)
			{
				int content = db[tableID]->itemAt(i,columnID[j]);
				if(content < left[j] || 
				   content > right[j])		//不在区间内
				{
					flag = false;
					break;
				}
			}
			if(flag)
				result->push_back(i);
		}
	}
	else		//操作表类
	{
		for(int i = 0;i<from->size();i++)
		{
			flag = true;
			for(int j = 0;j<columnID.size();j++)
			{
				int content = db[tableID]->itemAt((*from)[i],columnID[j]);
				if(content < left[j] 
				|| content > right[j])     //不在区间内
				{
					flag = false;
					break;
				}
			}
			if(flag)
				result->push_back((*from)[i]);
		}
	}
	return result;
}

vector<int>* Algorithm::select_Indexed( vector<int> &left, 
		vector<int> &right, CompoundIndex* index )
{
	int n = left.size();
	int *l = new int [n];
	int *r = new int [n];

	for (int i=0; i<n; ++i)
	{
		l[i] = left[i];
		r[i] = right[i];
	}
	vector<int> *result = new vector<int>();

	index->rangeQuery(l, r, *result);
	delete[] l;
	delete[] r;
	return result;
}

vector<vector<int>* > Algorithm::join_Bucket( int tableID1, 
				int columnID1, 
				int tableID2, 
				int columnID2, 
				vector<vector<int>* >& big, vector<int>& bigTables,
				vector<vector<int>* >& small, vector<int>& smallTables,
				Table *bigData, Table *smallData)
{
	vector<BucketPair> bucketSmall[BUCNUM];
	vector<vector<int>* > result(bigTables.size() + smallTables.size());

	for (int t=0; t<bigTables.size() + smallTables.size(); ++t)
		result[t] = new vector<int>();
	
	//找寻需要进行判断的table在所给表中分属第几列
	int bigPos, smallPos;
	for(int i = 0;i<bigTables.size();i++)		
		if(tableID1 == bigTables[i])
		{
			bigPos = i;
			break;
		}

	for(int i = 0;i<smallTables.size();i++)
		if(tableID2 == smallTables[i])
		{
			smallPos = i;
			break;
		}

	//扔到BUCNUM个桶中
	BucketPair temp;

	int smallSize;
	if ( small[0] )
		smallSize = small[0]->size();
	else
		smallSize = smallData->getNumberOfRow();

	for(int i = 0; i<smallSize; i++)
	{
		int rowID;
	        if ( small[smallPos] )
			rowID = (*small[smallPos])[i];
		else
			rowID = i;

		int value = smallData->itemAt(rowID, 0);

		int bucketNum = value % BUCNUM;					// bucketNum记录放进的桶的编号
		if (bucketNum<0) bucketNum += BUCNUM;
		temp.pos = i;
		temp.value = value;
		bucketSmall[bucketNum].push_back(temp);

	}
										// 每个桶做二重循环并判等
	int bigSize;
	if ( big[0] )
		bigSize = big[0]->size();
	else
		bigSize = bigData->getNumberOfRow();

	for(int i = 0; i<bigSize; i++)
	{
		int rowID;
		if ( big[bigPos] )
			rowID = (*big[bigPos])[i];
		else
			rowID = i;

		int value = bigData->itemAt(rowID, 0);

		int bucketNum = value % BUCNUM;					// bucketNum记录放进的桶的编号
		if (bucketNum<0) bucketNum += BUCNUM;

		vector<BucketPair>& currentBucket = bucketSmall[bucketNum];	// Loop over all smalls

		for (int j=0; j<currentBucket.size(); ++j)
		{
			if (value == currentBucket[j].value)			// They are really the same value
			{				
				for (int t=0; t<bigTables.size(); ++t)		// Add big
					if (big[t])
						result[t]->push_back( (*big[t])[i] );
					else
						result[t]->push_back( i );
	
				for (int t=0; t<smallTables.size(); ++t)	// Add small
					if (small[t])
						result[bigTables.size()+t]->
							push_back( (*small[t])[currentBucket[j].pos] );
					else
						result[bigTables.size()+t]->
							push_back( currentBucket[j].pos );
			}
		}
	}
	return result;
}

void qsortColumn( vector<vector<int>*> &a, int left, int right, int rowRef)
{
#define swapColumn(x, y) \
	for (int k=0; k<a.size(); ++k) \
		swap( (*a[k])[x], (*a[k])[y] );

	if (left>=right)
		return;

	int index = left;
	int pivot = (*a[rowRef])[index];

	swapColumn( index, right );
	for (int i=left; i<right; ++i)
	{
		if ( (*a[rowRef])[i]<pivot)
		{
			swapColumn( index, i );
			++index;
		}

	}
	swapColumn( right, index );

	qsortColumn(a, left, index-1, rowRef);
	qsortColumn(a, index+1, right, rowRef);
}

void sortColumn( vector<vector<int>*> &a, int rowRef )
{
	if (!a[0])
		return;

	qsortColumn(a, 0, a[0]->size()-1, rowRef);
}

void resultJoin(vector<int>& goodRows, vector<vector<int>*> &big, int bigPos, vector<pair<int,int> > indexResult)
{
	// goodRows = copy the rows (big[:][r]) from big (sorted by big[bigPos][r]), 
	// which big[bigPos][r] == indexResult[r].second

	vector<int> index2(indexResult.size());
	for (int i=0; i<indexResult.size(); ++i)
		index2[i] = indexResult[i].second;
	sort(index2.begin(), index2.end());

	if (!big[0])
	{
		goodRows = index2;
		return;
	}
	int S = big[0]->size();
	int i = 0, j = 0;

	goodRows.clear();
	while (i<S)
	{
		while (i<S && (*big[bigPos])[i] < index2[j]) ++i;
		while (i<S && (*big[bigPos])[i] == index2[j]) goodRows.push_back(i++);
		++j;
	}
}

vector<vector<int>* > Algorithm::join_Indexed( int tableID1, 
				int columnID1, 
				int tableID2, 
				int columnID2, 
				vector<vector<int>* > &big, vector<int> &bigTables,				
				vector<vector<int>* > &small, vector<int> &smallTables)
{
	// TODO
/*	assert( small[0] );
	vector<vector<int>* > result;
	for (int i=0; i<bigTables.size()+smallTables.size(); ++i)
		result.push_back( new vector<int>() );

	if ( small[0]->empty() )
	{
		return result;
	}

	//找寻需要进行判断的table在所给表中分属第几列
	int bigPos, smallPos;
	for(int i = 0;i<bigTables.size();i++)		
		if(tableID1 == bigTables[i])
		{
			bigPos = i;
			break;
		}

	for(int i = 0;i<smallTables.size();i++)
		if(tableID2 == smallTables[i])
		{
			smallPos = i;
			break;
		}

	// sort small by key(tableID2, columnID2)
	small.push_back(new vector<int>());
	for (int rs=0; rs<small[0]->size(); ++rs)
	{
		small.back()->push_back( db[tableID2]->itemAt( (*small[smallPos])[rs], columnID2) );
	}
	sortColumn( small, small.size()-1 );
	sortColumn( big, bigPos );

	// Ensure lastKey != (*small.back())[0] - 1
	int lastKey = (*small.back())[0] - 1;
	vector<pair<int, int> > indexResult;
	vector<int> goodRows;

	for (int rs=0; rs<small[0]->size(); ++rs)
	{
		// New key:
		if ( lastKey != (*small.back())[rs] )
		{
			lastKey = (*small.back())[rs];

			bigIndex->rangeQuery( lastKey, lastKey, indexResult );
			resultJoin(goodRows, big, bigPos, indexResult);
		}

		// Generate all (small, goodRows) pair
		for ( int rb=0; rb<goodRows.size(); ++rb )
		{
			for (int t=0; t<bigTables.size(); ++t)
				if ( big[0] == NULL )
					result[t]->push_back( goodRows[rb] );
				else
					result[t]->push_back( (*big[t])[goodRows[rb]] );

			for (int t=0; t<smallTables.size(); ++t)
				result[t+bigTables.size()]->push_back( (*small[t])[rs] );
		}
	}

	delete small.back();
	small.pop_back();
	return result;*/
}

vector<vector<int>* > Algorithm::join( int tableID1, 
				int columnID1, 
				int tableID2, 
				int columnID2, 
				vector<vector<int>* > &big, vector<int> &bigTables,				
				vector<vector<int>* > &small, vector<int> &smallTables,
				Table *bigData, Table *smallData
				)
{
	int S = big[0] ? big[0]->size() : db[tableID1]->getNumberOfRow();
	int Ss = small[0] ? small[0]->size() : db[tableID2]->getNumberOfRow();
	int hashIO = (S-1) / (MM::PageSize / sizeof(pair<int,int>)) + 1;
	
	//printf("Join: indexed=%d vs hash=%d\n", Ss, hashIO);

	//if ( small[0]==NULL || Ss > hashIO/10 || bigIndex==NULL )
	if (1)
	{
		return join_Bucket(tableID1, columnID1, tableID2, columnID2,
				big, bigTables, small, smallTables, bigData, smallData);
	}
}

vector<vector<int>* > Algorithm::cartesianProduct( 
		int bigTableID,
		int smallTableID,
		vector<vector<int>* > &big,  
		vector<vector<int>* > &small )
{
	vector<vector<int>* > result;

	for (int i=0; i<big.size() + small.size(); ++i)
		result.push_back( new vector<int>() );

	/// assert: if big[t] is null, big.size()==1 and t==0
	int bigSize;
	if ( big[0] )
		bigSize = big[0]->size();
	else
		bigSize = db[bigTableID]->getNumberOfRow();

	int smallSize;
	if ( small[0] )
		smallSize = small[0]->size();
	else
		smallSize = db[smallTableID]->getNumberOfRow();

	for(int i=0; i<bigSize; i++)
	{
		for(int j=0; j<smallSize; j++)
		{
			for (int k=0; k<big.size(); ++k)
				if (big[k])
					result[k]->push_back( (*big[k])[i] );
				else
					result[k]->push_back( i );

			for (int k=0; k<small.size(); ++k)
				if (small[k])
					result[k+big.size()]->push_back( (*small[k])[j] );
				else 
					result[k+big.size()]->push_back( j );
		}
	}
	return result;
} 

void Algorithm::test()
{
	// Set up DB data

	/*vector<bool> cached;
	cached.push_back(1); cached.push_back(1); cached.push_back(0); cached.push_back(0);
	db.push_back(new Table(cached));

	cached.clear();
	cached.push_back(1); cached.push_back(1); cached.push_back(0);
	db.push_back(new Table(cached));

	Table::setRowCacheSize(16384*10);

	Record record = new int[4];
	record[0] = 1; record[1] = 1; record[2] = 1; record[3] = 1;		db[0]->push(record);
	record[0] = 2; record[1] = 1; record[2] = 2; record[3] = 100;		db[0]->push(record);
	record[0] = 3; record[1] = 1; record[2] = 1; record[3] = 1000;		db[0]->push(record);
	record[0] = 4; record[1] = 2; record[2] = 2; record[3] = 10000;		db[0]->push(record);
	record[0] = 5; record[1] = 2; record[2] = 3; record[3] = 100000;	db[0]->push(record);
	
	record[0] = 1; record[1] = 1; record[2] = 1; 				db[1]->push(record);
	record[0] = 2; record[1] = 2; record[2] = 3; 				db[1]->push(record);
	record[0] = 3; record[1] = 1; record[2] = 4; 				db[1]->push(record);
	record[0] = 4; record[1] = 2; record[2] = 5; 				db[1]->push(record);

	int tableID = 0;
	vector<int> columnID;
	vector<int> left;
	vector<int> right;
	vector<int>* from;

	/// Test select_BruteForce
	columnID.push_back(3); left.push_back(100); right.push_back(100000); 
	columnID.push_back(2); left.push_back(2); right.push_back(3);

	from = select_BruteForce(tableID, columnID, left, right, NULL);

	puts("Results:");
	for (int i=0; i<from->size(); ++i)
		printf("%d\t",(*from)[i]);
	printf("\n");

	/// Test select Indexed
	from = select_Indexed(tableID, 1, 1, 6, from);

	puts("Results:");
	for (int i=0; i<from->size(); ++i)
		printf("%d\t",(*from)[i]);
	printf("\n");
	
	/// Test join_Bucket
	vector<vector<int>*> big, small, res;
	vector<int> bigTables, smallTables;
	int tableID1 = 0, columnID1 = 1;
	int tableID2 = 1, columnID2 = 1;

	bigTables.push_back(0);
	smallTables.push_back(1);

	big.push_back(from);
	small.push_back(from);

	res = join_Bucket(tableID1, columnID1, tableID2, columnID2, 
			big, bigTables,
			small, smallTables);

	puts("Results:");
	for (int i=0; i<res[0]->size(); ++i)
	{
		for (int j=0; j<res.size(); ++j)
			printf("%d\t", (*res[j])[i]);
		printf("\n");
	}*/
}

/*
vector<vector<int> > Algorithm::project(vector<int> desiredTables, 
       			vector<int> desiredColumns, 
 				vector<vector<int> >ID, 
 				vector<int> tables)
{
	//TODO: @Ruobing 映射
	vector<vector<int> > result;
	vector<int> tableMap;
	for(int i = 0;i<desiredTables.size();i++)		//建立table对应连接
	{
		for(int j = 0;j<tables.size();j++)
		{
			if(desiredTables[i] == tables[j])
			{
				tableMap.push_back(j);
				break;
			}
		}
	}

	for(int i = 0;i<ID.size();i++)		//映射
	{
		vector<int> temp;
		for(int j = 0;j<desiredTables.size();j++)
		{
			temp.push_back(db[desiredTables[j]]->itemAt(ID[i][tableMap[j]],desiredColumns[j]));
		}
		result.push_back(temp);
	}
	return result;
}
*/

