#ifndef __UTIL
#define __UTIL

#include <vector>
#include <string>
#include <cstring>
#include "table.h"
#include "structs.h"
using namespace std;

const int INF = 2147483647;
const long long rowCacheSize = 200LL * 1024 * 1024;
const long long stringCacheSize = 20LL * 1024 * 1024;
const double REQUIRE_WEIGHT = 0.0;
const double SELECT_WEIGHT = 2.0;
const double JOIN_WEIGHT = 1.0;

extern char readBuffer[65536];

void writeChar(char* buff, int& pos, char c)
{
	buff[pos++] = c;
}

void writeInteger(char* buff, int& pos, int n)
{
	if (n==0)
	{
		buff[pos++]='0';
		return;
	}
	if (n<0)
	{
		buff[pos++]='0';
		n = -n;
	}

	int startPos = pos;
	while (n) 
	{
		buff[pos++]='0'+n%10;
		n/=10;
	}
	int i, j;
	char tmp;
	for (i=startPos, j=pos-1; i<j; ++i, --j) {
		tmp = buff[i];
		buff[i] = buff[j];
		buff[j] = tmp;
	}
}

int toInt(string str)
{
	int res = 0;
	for (int i=0; i<str.size(); ++i)
		res = res*10 + str[i] - '0';
	return res;
}

void toCString(string str, char *buff)
{
	int n = str.size();
	for (int i=0; i<n; ++i)
		buff[i] = str[i];
	buff[n] = '\n';
}


int resultRows(int t, vector<int>* records)
{
	if (records==NULL)
		return db[t]->getNumberOfRow();
	else
		return records->size();
}


int resultRows(int t, vector<vector<int>*> &records)
{
	return resultRows(t, records.front());
}

void chrisTokenize(string buf, vector<string>& token)
{
	char temp[65536];
	int i, j;

	token.clear();
	j = 0;

	for (i = 0; buf[i]; i++) {
		if (buf[i] == ' ') {
			temp[j] = '\0';
			token.push_back(temp);
			j = 0;
		} else {
			temp[j++] = buf[i];
		}
	}

	temp[j] = '\0';
	token.push_back(temp);
}

pair<int, int> makeRange(string op, string sa)
{
	int a = toInt( sa );

	if (op=="=") return make_pair(a, a);
	if (op=="<") return make_pair(-INF-1, a-1);

	return make_pair(a+1, INF);
}

pair<int, int> setJoin(pair<int, int> a, pair<int, int> b)
{
	return make_pair( max(a.first, b.first), min(a.second, b.second) );
}

bool isNumber(string str)
{
	for (int i=0; i<str.size(); ++i)
		if (!isdigit(str[i]))
			return false;
	return true;
}

bool isStringConst(string str)
{
	return (str[0] == '\'') && (str[str.size()-1] == '\'');
}

void free(vector<vector<int>*> &v)
{
	for (int i=0; i<v.size(); ++i)
		delete v[i];
	v.clear();
}

int root(int x, vector<int>& father)
{
	while ( father[x]!=-1 ) x = father[x];
	return x;
}

string toString(int n)
{
	sprintf(readBuffer, "%d", n);
	return string(readBuffer, readBuffer+strlen(readBuffer));
}

#endif
