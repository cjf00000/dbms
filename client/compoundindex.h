#ifndef __COMPOUND_INDEX
#define __COMPOUND_INDEX

#include "index.h"

class SubIndex;
class MM;

class CompoundIndex : public Index
{
public:
	static const int MergeScaleFactor = 5;
	static const int BlockListCapacity = 30000;

	CompoundIndex(MM* memory, int keyLength);

	/// Basic operations
	void push(int *key, int value);

	void rangeQuery(int *lower, int *upper,
			vector<int>& result);

	void initialize();

	int usage();

	/// Convert a BlockList to DiskArray
	void shrink();

	virtual ~CompoundIndex();

	void test();

private:
	void printChain();

	static bool worthMerge(SubIndex *small, SubIndex *large);

#ifdef DEBUG_MODE
public:
#endif
	vector<SubIndex*> index;
#ifdef DEBUG_MODE
private:
#endif

	bool init;

	int blockListID();
	int keyLength;

	MM* memory;
};

#endif
