#ifndef __BLOCKLIST
#define __BLOCKLIST

#include "subindex.h"
#include <list>

class Block;

class BlockList : public SubIndex
{
public:
	BlockList(int keyLength);

	/// Basic operations
	void push(int* key, int value);

	void rangeQuery(int* lower, int* upper,
		vector<int>& result);

	void getKth(int k, int *ret);

	virtual int usage();

	virtual ~BlockList();

	void begin();
	void next(int *ret);

private:

	list<Block*> data;
	list<Block*>::iterator aIter;
	int aOffset;
	int keyLength;
	int recordSize;
	CompareArray* compare;
};

#endif
