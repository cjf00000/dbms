#include "subindex.h"
#include <cassert>
#include <algorithm>
#include "mm.h"
#include "blocklist.h"
#include "diskarray.h"
#include "table.h"
#include <cmath>
#include <memory.h>

int* clone(int* source, int length)
{
	int* ret = new int[length];
	memcpy(ret, source, length*sizeof(int));
	return ret;
}

DiskArray::DiskArray(MM* memory, int keyLength) : SubIndex()
{
	Type = 1;
	ordered = false;

	table = new Table(memory, keyLength+1);
	this->memory = memory;

	this->keyLength = keyLength;
	this->DABlockItems = MM::PageSize / (sizeof(int)*(keyLength+1));
	myCompare = new CompareArray(keyLength);
}

int tmp[256];

void DiskArray::push(int* key, int value)
{
	assert(!ordered);

	memcpy(tmp, key, keyLength*sizeof(int));
	tmp[keyLength] = value;

	table->push(tmp);
	++N;
}

void DiskArray::qsortMe()
{
	int **temp = new int *[N];
	for (int i=0; i<N; ++i)
		temp[i] = new int [keyLength+1];

	// ---- Load:
	for (int i=0; i<N; ++i)
	{
		int* st = (int*)table->itemAt(i);
		memcpy(temp[i], st, (keyLength+1)*sizeof(int));
	}

	// ---- Sort:
	sort(temp, temp+N, *myCompare);	

	// ---- Save:
	Table* newTable = new Table(memory, keyLength+1);
	for (int i=0; i<N; ++i)
		newTable->push(temp[i]);

	// ---- Compute first and last
	assert(first.empty());
	first.clear();
	last.clear();
	int totalPages = (N-1)/DABlockItems + 1;
	for (int i=0; i<totalPages-1; ++i)
	{
		first.push_back( clone(temp[i*DABlockItems], keyLength) );
		last.push_back( clone(temp[(i+1)*DABlockItems-1], keyLength) );
	}
	// Last page is not full...
	first.push_back( clone(temp[ (totalPages-1)*DABlockItems ], keyLength) );
	last.push_back( clone(temp[N-1], keyLength) );

	// ---- Swap table
	delete table;
	table = newTable;

	for (int i=0; i<N; ++i)
		delete[] temp[i];
	delete[] temp;
}

DiskArray* DiskArray::mergeSort(int l, int r)
{
	if (r-l+1 < MemThreshold)
	{
		DiskArray *newArray = new DiskArray(memory, keyLength);
		int *ret = new int[keyLength+1];
		for (int i=l; i<=r; ++i)
		{
			getKth(i, ret);
			newArray->push(ret, ret[keyLength]);
		}
		newArray->initialize();
		delete[] ret;
		return newArray;
	}
	int mid = (l+r)/2;
	DiskArray *a1 = mergeSort(l, mid-1);
	DiskArray *a2 = mergeSort(mid, r);

	a1->mergeWith(a2);
	delete a2;
	return a1;
}

void DiskArray::initialize()
{
	// TODO: change this to external sort
	assert(!ordered);
	ordered = true;

	if (!N) return;
	if (N<MemThreshold) qsortMe();
	else {
		DiskArray *result = mergeSort(0, N-1);
		
		delete table;
		table = result->table;
		first = result->first;
		last = result->last;
		delete result->myCompare;

	/*	begin();
		int *key = new int[keyLength+1];
		for (int i=0; i<N; ++i)
		{
			next(key);
			for (int j=0; j<keyLength+1; ++j)
				printf("%d\t", key[j]);
			printf("\n");
		}
		delete[] key;*/
/*		for (int i=0; i<first.size(); ++i)
		{
			for (int j=0; j<keyLength; ++j)
				printf("%d\t", first[i][j]);
			printf("\n");
		}
		exit(0);*/
	}
}

void DiskArray::rangeQuery(int* lower, int* upper,
		vector<int>& result)
{
	assert(ordered);

	result.clear();

/*	for (int i=0; i<first.size(); ++i)
	{
		for (int k=0; k<keyLength; ++k)
			printf("%d\t", first[i][k]);
		printf("\n");
		for (int k=0; k<keyLength; ++k)
			printf("%d\t", last[i][k]);
		printf("\n");
	}*/
	
	int vb = lower_bound(last.begin(), last.end(), lower, *myCompare) - last.begin();
	int ve = upper_bound(first.begin(), first.end(), upper, *myCompare) - first.begin();

	int offset = vb * DABlockItems;
//	printf("vb=%d, ve=%d\n", vb, ve);
	for (int i=vb; i<ve; ++i, offset+=DABlockItems)
	{
		int Size = (i+1==first.size() ? N%DABlockItems : DABlockItems);

		int* res = table->itemAt(offset);

		for (int j=0; j<Size; ++j)
		{
			if ( !(*myCompare)(res, lower) && !(*myCompare)(upper, res) )
			{
				result.push_back( res[keyLength] );
			}
			res += (keyLength+1);
		}
	}
}

void DiskArray::getKth(int k, int *ret)
{
	assert( ordered );
	memcpy( ret, table->itemAt(k), (keyLength+1)*sizeof(int) );
}

void DiskArray::mergeWith(SubIndex *subIndex)
{
	Table *newTable = new Table(memory, keyLength+1);

	int i = 0, j = 0;
	subIndex->begin();
	int *currentJ = new int[keyLength+1];
	int *item = new int[keyLength+1];
	subIndex->next(currentJ);

	int cnt = 0;

	for (int i=0; i<first.size(); ++i) delete[] first[i];
	for (int i=0; i<last.size(); ++i) delete[] last[i];
	first.clear();
	last.clear();

	int* currentIPage = new int[MM::PageSize]; 
	int* currentI;
	bool iDead = 0, jDead = 0;

	if (N>0)
	{
		memcpy(currentIPage, table->itemAt(0), MM::PageSize);
		currentI = currentIPage;
	}
	else iDead = 1;

	int cnt2 = 0, offset = 0;

	while (i<N || j<subIndex->N)
	{
		if ( jDead || !iDead && !(*myCompare)(currentJ, currentI) )
		{
			memcpy(item, currentI, (keyLength+1)*sizeof(int));
			++i;

			if (i<N) {
				if ( (++cnt2)%DABlockItems == 0 )			//reload
				{
					offset += DABlockItems;
					memcpy(currentIPage, table->itemAt(offset), MM::PageSize);
					currentI = currentIPage;
				}
				else currentI += (keyLength+1);
			}
			else iDead = 1;
		}
		else 
		{
			memcpy(item, currentJ, (keyLength+1)*sizeof(int));
			++j;

			if (j<subIndex->N) subIndex->next(currentJ);
			else jDead = 1;
		}
		newTable->push(item);

		if ( cnt % DABlockItems == 0 )
		{
			first.push_back( clone(item, keyLength) );
		}

		if ( cnt % DABlockItems == DABlockItems-1 )
		{
			last.push_back( clone(item, keyLength) );
		}

		++cnt;
	}
	if ( cnt % DABlockItems > 0 )
		last.push_back( clone(item, keyLength) );

	delete table;
	table = newTable;

	N += subIndex->N;

	delete[] currentIPage;
	delete[] currentJ;
	delete[] item;
}

int DiskArray::usage()
{
	return 0;
}

DiskArray::~DiskArray()
{
	delete table;
	delete myCompare;
	for (int i=0; i<first.size(); ++i)
	{
		delete[] first[i];
		delete[] last[i];
	}
}

void DiskArray::begin()
{
	aOffset = 0;
}

void DiskArray::next(int *ret)
{
	getKth(aOffset++, ret);
	//return (++aOffset == N);
}

void DiskArray::test()
{
	int keyLength = 4;

	BlockList *blockList = new BlockList(keyLength);

	puts("Insert");
	for (int iter = 0; iter < 20000; ++ iter )
	{
		int *a = new int[keyLength];
		int value = rand()%5;
		for (int i=0; i<keyLength; ++i) {
			a[i] = rand()%5;
		}
		push(a, value);
		blockList->push(a, value);
	}

	puts("Initialize");
	initialize();

	puts("Insert2");
	BlockList *blockList2 = new BlockList(keyLength);
	for (int iter = 0; iter < 20000; ++ iter )
	{
		int *a = new int[keyLength];
		int value = rand()%5;
		for (int i=0; i<keyLength; ++i) 
			a[i] = rand()%5;
		blockList->push(a, value);
		blockList2->push(a, value);
	}

	puts("Merge");
	DiskArray* diskArray2 = new DiskArray(memory, keyLength);
	diskArray2->initialize();
	diskArray2->mergeWith(blockList2);

	puts("Merge2");
	mergeWith(diskArray2);

	puts("Range Query");
	for (int iter = 0; iter < 100000; ++iter )
	{
		if ((iter & 1023) == 0)
		{
			printf(".");
			fflush(stdout);
		}

		int *upper = new int[keyLength];
		int *lower = new int[keyLength];
		for (int i=0; i<keyLength; ++i)
			upper[i] = lower[i] = rand()%5;

		lower[keyLength-1] = rand()%5;
		upper[keyLength-1] = rand()%5;

		if (lower[keyLength-1] > upper[keyLength-1])
			swap(lower[keyLength-1], upper[keyLength-1]);

		vector<int> resultStd, result;

		rangeQuery(lower, upper, result);
		blockList->rangeQuery(lower, upper, resultStd);

		int checkSum = 0;
		for (int i=0; i<result.size(); ++i)
			checkSum += result[i];

		for (int i=0; i<resultStd.size(); ++i)
			checkSum -= resultStd[i];

		if (checkSum)
		{
			puts("Error!");
			puts("Query");
			for (int i=0; i<keyLength; ++i)
				printf("(%d, %d)\t", lower[i], upper[i]);
			printf("\n");
			for (int i=0; i<result.size(); ++i)
				printf("%d\n", result[i]);
			puts("----");
			for (int i=0; i<resultStd.size(); ++i)
				printf("%d\n", resultStd[i]);
			exit(0);
		}
	}

	puts("\nFinished.");
}
