#include "table.h"
#include <memory.h>
#include "mm.h"

vector<Table*> db;

Table::Table(MM* memory, int C)
{
	//ENTER("Table()");
	this->R = 0;
	this->C = C;
	this->recordSize = C * sizeof(int);
	this->recordPerPage = MM::PageSize / recordSize;
	this->memory = memory;

	lastPPN = -1;
	lastPageBegin = new int[MM::PageSize/sizeof(int)];
}

Record Table::itemAt(int r)
{
	//ENTER("itemAt: mode r");
	pair<int, int> ppnAndOffset = locate(r);
	
	int* pageBegin;
	
	if (ppnAndOffset.first == lastPPN)
	{
		pageBegin = lastPageBegin;
	}
	else
	{
		lastPPN = ppnAndOffset.first;
       		memcpy(lastPageBegin, memory->getPage(ppnAndOffset.first), MM::PageSize);
		pageBegin = lastPageBegin;	
	}

	return pageBegin + ppnAndOffset.second * C;
}

int Table::itemAt(int r, int c)
{
	//return 0;
	return itemAt(r)[c];
}

int Table::getNumberOfRow()
{
	//ENTER("getNumberOfRow");
	return R;
}

void Table::push(Record record)
{
	int offset = R % recordPerPage;

	if (offset == 0) 					// Create new page
		ppnLUT.push_back( memory->newPage() );

	pair<int, int> ppnAndOffset = locate(R);		// Load existing page

	int *lastPage = (int*)memory->getPage( ppnAndOffset.first );
	memory->setPageDirty( ppnAndOffset.first );		// Set dirty bit
	
	memcpy( lastPage + offset*C, record, recordSize);	// Copy data

	R++;

	lastPPN = -1;
}

pair<int, int> Table::locate(int r)
{
	int offset = r % recordPerPage;
	int lpn = r / recordPerPage;
	int ppn = ppnLUT[lpn];

	return make_pair(ppn, offset);
}

void Table::test()
{
	//ENTER("test");
	// ---- Do initialization

	MM* mm = new MM("data/chris.pageFile", 8192*10000);

	// Create DB
	db.push_back(new Table(mm, 3));
	db.push_back(new Table(mm, 1));

	vector< vector<int*> > realDB(2);
	vector<int> nCol;
	nCol.push_back(3);
	nCol.push_back(1);
	for (int i=0; i<10000000; ++i)
	{
		int nDB = rand()%2;
		// printf("%d\n", i);
		if (rand()%2)
		{
			// printf(">> Insert %d\n", db[0]->getNumberOfRow());
			int *record = new int [nCol[nDB]];
			realDB[nDB].push_back(record);
			for (int j=0; j<nCol[nDB]; ++j)
				record[j] = rand();
			db[nDB]->push(record);
		}
		else if (!realDB[nDB].empty())
		{
			int fetched_num = db[nDB]->getNumberOfRow();
			if (fetched_num != realDB[nDB].size())
			{
				printf("Error: num not same: expected %d but got %d\n", 
						realDB[0].size(), db[0]->getNumberOfRow());
			}
			int r=rand()%fetched_num;
			int c=rand()%nCol[nDB];
			// printf(">> Access %d %d\n", r, c);

			if (db[nDB]->itemAt(r, c) != realDB[nDB][r][c])
			{
				printf("Error in item (%d, %d): expected %d but got %d. Database is %d now.\n", 
						r, c, realDB[nDB][r][c], db[nDB]->itemAt(r, c), fetched_num);
			}
			// printf("DB size: %d\n", fetched_num);
		}
	}

	printf("Finished.\n");
}

Table::~Table()
{
	delete[] lastPageBegin;
}
