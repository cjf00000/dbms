#include "compoundRestriction.h"
#include <cassert>
#include "compoundindex.h"
#include "mm.h"
#include <cstdio>

bool operator == (CompoundRestrictionItem &a, CompoundRestrictionItem &b)
{
	if (a.e.size()!=b.e.size()) 
		return false;
	sort(a.e.begin(), a.e.end());
	sort(b.e.begin(), b.e.end());

	for (int i=0; i<a.e.size(); ++i)
		if (a.e[i]!=b.e[i])
			return false;
	return true;
}

MM* CompoundRestrictionItem::memory;

CompoundRestriction::CompoundRestriction(int numColumn)
{
	for (int i=0; i<numColumn; ++i)
		combs.push_back(CompoundRestrictionItem(vector<int>(), i));
}

CompoundRestrictionItem::CompoundRestrictionItem(vector<int> e, int ie)
{
	this->e = e;
	if (ie!=-1) this->e.push_back(ie);
	this->index = new CompoundIndex(memory, size());
}

bool isEqual(ColumnRestriction restriction)
{
	return restriction.range.first == restriction.range.second;
}

CompoundRestrictionItem CompoundRestriction::toItem(vector<ColumnRestriction> &restriction)
{
	vector<int> e;
	int ie = -1;
	for (int i=0; i<restriction.size(); ++i)
		if (isEqual(restriction[i]))
			e.push_back(restriction[i].ID.columnID);
		else
			ie = restriction[i].ID.columnID;

	return CompoundRestrictionItem(e, ie);
}

void CompoundRestriction::trainItem(vector<ColumnRestriction> &restriction)
{
	CompoundRestrictionItem item = toItem(restriction);
/*	printf("Train: e=");
	for (int i=0; i<item.e.size(); ++i) printf("%d\t", item.e[i]);
	printf("\n");*/

	for (int i=0; i<combs.size(); ++i)
		if (combs[i]==item)
			return;
	combs.push_back( item );
}

bool vectorContains(vector<int> a, vector<int> b)
{
	sort(a.begin(), a.end());
	sort(b.begin(), b.end());

	int j=0;
	for (int i=0; i<b.size(); ++i)
	{
		while (j<a.size() && a[j]!=b[i]) ++j;
		if (j==a.size())
			return false;
	}
	return true;
}

CompoundRestrictionItem& CompoundRestriction::optimumItem(vector<ColumnRestriction> &restriction)
{
	vector<int> e;
	int ie = -1;
	for (int i=0; i<restriction.size(); ++i)
		if (isEqual(restriction[i]))
			e.push_back(restriction[i].ID.columnID);
		else
			ie = restriction[i].ID.columnID;

	int bestSize = 0;
	int bestItem;

	e.push_back(ie);
	for (int i=0; i<combs.size(); ++i)
		if (ie==-1 || combs[i].e.back()==ie)
		{
			bool cover = vectorContains(e, combs[i].e);

			if (cover)
			{
				if (combs[i].size()>bestSize)
				{
					bestSize = combs[i].size();
					bestItem = i;
				}
			}
		}

	return combs[bestItem];
}

void CompoundRestriction::test()
{
	MM* memory = new MM("data/chris.pageFile", 40960);
	vector<int> e;
	int ie;
	vector<ColumnRestriction> vr;
	CompoundRestrictionItem::memory = memory;

	CompoundRestriction res = CompoundRestriction(3);
	vr.push_back( ColumnRestriction(GlobalColumnID(0,0), make_pair(3,3)) );
	vr.push_back( ColumnRestriction(GlobalColumnID(0,1), make_pair(1,1)) );
	vr.push_back( ColumnRestriction(GlobalColumnID(0,2), make_pair(2,2)) );

	res.optimumItem(vr).print();


}
