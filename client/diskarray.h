#ifndef __DISKARRAY
#define __DISKARRAY

#include "subindex.h"
#include <cstdio>

class BlockList;
class Table;
class MM;
class CompareArray;

class DiskArray : public SubIndex
{
	static const int MemThreshold = 400LL * 1024 * 1024;
//	static const int MemThreshold = 40960;
public:
	DiskArray(MM *memory, int keyLength);

	/// Basic operations
	/// push: only used when disordered, otherwise, use mergeWith(BlockList)
	void push(int* key, int value);

	void rangeQuery(int* lower, int* upper,
		vector<int>& result);

	virtual ~DiskArray();

	void mergeWith(SubIndex *subIndex);

	/// load all disordered records, sort, and save back
	void initialize();
	void qsortMe();
	DiskArray* mergeSort(int l, int r);

	void getKth(int k, int *ret);

	int usage();

	virtual void test();

	void begin();
	void next(int *ret);

private:
	/// 0 = Disordered
	/// 1 = ordered
	bool ordered;

	/// Value of last item in block
	/// Note: this value of the last block may be inaccurate
	vector<int*> first, last;

#ifdef DEBUG_MODE
public:
#endif
	Table* table;
	MM* memory;

	int aOffset;
	int keyLength;
	int DABlockItems;
	CompareArray *myCompare;
};

#endif
