#include "blocklist.h"
#include <algorithm>
#include <cstdio>
#include <iostream>
#include "mm.h"
#include <memory.h>

const int BlockCapacity = 512;

struct Block
{
	int *data;
	int size;
	int recordSize;

	Block(int recordSize)
	{
		size = 0;
		this->recordSize =recordSize;
		data = new int[BlockCapacity*recordSize];
	}

	~Block()
	{
		delete[] data;
	}

	int* last() { return data + (size-1)*recordSize; }
	int* first() { return data; }	
	int* kth(int k){ return data + k*recordSize; }
};

BlockList::BlockList(int keyLength) : SubIndex()
{
	Type = 0;
	this->keyLength = keyLength;
	this->recordSize = keyLength + 1;
	this->compare = new CompareArray(keyLength);
}

BlockList::~BlockList()
{
	for (list<Block*>::iterator iter = data.begin(); iter != data.end(); ++iter)
		delete *iter;
}

void BlockList::push(int *key, int value)
{
	++N;

	// Empty list
	if (data.begin() == data.end())
	{
		data.push_back( new Block(recordSize) );
		data.front()->size ++;
		memcpy(data.front()->first(), key, keyLength*sizeof(int));
		data.front()->first()[keyLength] = value;
		return;
	}

	list<Block*>::iterator iter;
	for (iter = data.begin(); iter != data.end(); ++iter)
	{
		if ( !(*compare)((*iter)->last(), key) )
			break;
	}

	// key is greater than all items
	if (iter == data.end())
		--iter;

	// Split
	if ( (*iter)->size == BlockCapacity)
	{
		list<Block*>::iterator niter = iter;
		niter++;
		niter = data.insert(niter, new Block(keyLength+1));

		Block* current = *iter;
		Block* next = *niter;

		current->size = next->size = BlockCapacity/2;
		memcpy( next->data, current->data + BlockCapacity/2 * recordSize, 
				BlockCapacity/2 * recordSize * sizeof(int) );

		if ( (*compare)(current->last(), key) )
			++iter;
	}

	// Insert in current block
	Block* current = *iter;
	int p = 0;
	while ( !(*compare)(key, current->kth(p))
		&& p<current->size ) ++p;

	for (int q=current->size; q>p; --q)
		memcpy(current->kth(q), current->kth(q-1), recordSize*sizeof(int));

	memcpy(current->kth(p), key, keyLength*sizeof(int));
	current->kth(p)[keyLength] = value;
	current->size ++;
}

void BlockList::rangeQuery(int *lower, int *upper, 
			vector<int>& result)
{
	result.clear();
	for (list<Block*>::iterator iter = data.begin(); iter != data.end(); ++iter)
	{
		if ( (*compare)(upper, (*iter)->first()) )
			break;

		if ( !(*compare)( (*iter)->last(), lower ) )
		{
			Block* current = *iter;
			for (int i=0; i<current->size; ++i)
				if ( !(*compare)(current->kth(i), lower)
				&& !(*compare)(upper, current->kth(i)) )
				{
					result.push_back( current->kth(i)[keyLength] );
				}
		}
	}
}

void BlockList::getKth(int k, int *ret)
{
	list<Block*>::iterator iter;
	for (iter = data.begin(); iter != data.end(); ++iter)
		if ( k < (*iter)->size )
			break;
		else
			k -= (*iter)->size;

	memcpy( ret, (*iter)->kth(k), recordSize*sizeof(int) );
}

// Simple iterator
void BlockList::begin()
{
	aIter = data.begin();
	aOffset = 0;
}

void BlockList::next(int *ret)
{
	memcpy( ret, (*aIter)->kth(aOffset), recordSize*sizeof(int));

	if ( (++aOffset) == (*aIter)->size )
	{
		aIter++;
		aOffset = 0;
	}
}

int BlockList::usage()
{
	return data.size() * ( sizeof(Block) + 100 );
}
