/**
	String
	====

	Since the table only composed of int field, string fields are converted to int fields and store.

	Each string field is saved as:
	hash	c0~3	c4~7	c8~11	...
*/
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <map>
#include <cassert>
#include <algorithm>

#include "../include/client.h"

#include <stdio.h>  
#include <memory.h>
#include "table.h"
#include "algorithm.h"
#include "stringLUT.h"
#include <iostream>
#include "mm.h"
#include "compoundindex.h"
#include <queue>
#include "parser.h"
#include "subindex.h"
#include "diskarray.h"
#include "compoundRestriction.h"

using namespace std;
using namespace Algorithm;

/// Total number of tables
int T=0;

map<string, int > tableToTableID;
map<string, int > columnToColumnID;
map<string, int > columnToTableID;

/// Number of actual fields
vector<int> numColumns;

/// Each item in a row vector (r_t1, r_t2, ..., r_tk), which means that
/// the true result in the union of r_t1 th row in table[t1], r_t2 th row in table[t2], ...
vector<vector<int>* > resultItems;

/// t1, t2, ...
vector<int> resultTables;

int currentRow;

vector<GlobalColumnID> selectedColumns;

/// Used to double-check the results when next() if hash compasison goes wrong
vector<StringRestriction> stringRestriction;

vector<vector<int> > columnSLength;
vector<vector<int> > columnIsString;

vector<StringLUT*> stringLUT;

MM *memory, *stringMemory;
vector<vector<bool> > isPrimaryKey;
vector<CompoundRestriction> comres;
vector<vector<Table*> > clusterData;
bool initialized;

void create(const string& table, const vector<string>& column,
	const vector<string>& type, const vector<string>& key)
{
	T++;

	int currentTableID = tableToTableID.size();
	tableToTableID[table] = currentTableID;				// Init table name -> table ID


	vector<int> sLength;
	vector<int> sa;	
	for (int i=0; i<column.size(); ++i)
	{
		columnToTableID[column[i]] = currentTableID;		// Init column name -> table ID
		columnToColumnID[column[i]] = i;			// Init column name -> column ID

		if (type[i] != "INTEGER")				// Initialize string: field length
		{
			int sl = toInt( type[i].substr(8, type[i].size()-9) );
			sLength.push_back(sl);
			sa.push_back(true);
		}
		else {
			sLength.push_back(0);
			sa.push_back(false);
		}
	}
	
	vector<bool> myPrimary(column.size(), false);
	for (int i=0; i<key.size(); ++i)
		myPrimary[columnToColumnID[key[i]] ] = true;

	columnSLength.push_back(sLength);
	columnIsString.push_back(sa);
	numColumns.push_back(column.size());
	isPrimaryKey.push_back(myPrimary);
}

void train(const vector<string>& query, const vector<double>& weight)
{
	// TODO external sort
	memory = new MM("data/chris.pageFile", rowCacheSize);
	stringMemory = new MM("data/string.pageFile", stringCacheSize);
	CompoundRestrictionItem::memory = memory;
	for (int i=0; i<T; ++i)
		comres.push_back(CompoundRestriction(numColumns[i]));

	/// Column cache benefit[i] = 
	///	(2*numberOfSelects + 1*numberOfJoins + 0.2*numberOfRequires) / columnSize

	for (int i=0; i<query.size(); ++i)
	{
		vector<string> tokens;
		chrisTokenize(query[i], tokens);
		if (tokens[0]=="INSERT")
			continue;

		vector<int> fromTableID;
		vector<ColumnRestriction> restriction;
		vector<pair<GlobalColumnID, GlobalColumnID> > equalPairs;
		
		bool emptyResult = parseSelect(tokens, selectedColumns, fromTableID,
					restriction, equalPairs, stringRestriction);

		vector<vector<ColumnRestriction> > restriction_table(T);
		for (int j=0; j<restriction.size(); ++j)
		{
			restriction_table[restriction[j].ID.tableID].push_back(
					restriction[j]);
		}
		for (int j=0; j<T; ++j)
			if (!restriction_table[j].empty())
				comres[j].trainItem(restriction_table[j]);
	}

	for(int i=0;i<T;++i)						// Create Each Table
	{
		Table* tb= new Table(memory, numColumns[i]);
		db.push_back(tb);

		// Create string LUT of this table
		stringLUT.push_back(
				new StringLUT( stringMemory, columnSLength[i] ) );
	}
}

char readBuffer[65536];
char columnContentBuffer[40][65536];

///	Insert each row into table
void load(const string& table, const vector<string>& row)
{
	int currentTable = tableToTableID[table];
	char buff[65536];

	for (int r=0; r<row.size(); ++r)			// Read each row
	{
		Record record = new int[numColumns[currentTable]];

		string currentRow = row[r] + ',';
		toCString(currentRow, buff);
		int readPos = 0;

		vector<char*> stringsToInsert;
		for (int c=0; c<numColumns[currentTable]; ++c)
		{
			int numValue;
			int numArgsRead = sscanf(buff+readPos, "%d,", &numValue);

			if (numArgsRead)			// INTEGER
			{
				record[ c ] = numValue;

				memset(columnContentBuffer[c], 0, 4);
				stringsToInsert.push_back(columnContentBuffer[c]);
			}
			else {					// STRING
				while (buff[readPos]!='\'')
					++readPos;

				int nextPos = readPos + 1;
				while (buff[nextPos]!='\'')
					++nextPos;

				memcpy(columnContentBuffer[c], 
						buff+readPos+1, nextPos-readPos-1);
				columnContentBuffer[c][nextPos-readPos-1] = '\0';

				stringsToInsert.push_back(columnContentBuffer[c]);

				// Hash the string
				record[ c ] = StringLUT::hash(columnContentBuffer[c]);
			}

			while (buff[readPos]!=',') ++readPos;	// Move readPos forward, after next ','
			++readPos;
		}

		if (initialized)
		{
			int *key = new int[numColumns[currentTable]];
			for (int ncomb=0; ncomb<comres[currentTable].combs.size(); ++ncomb)
			{
				CompoundRestrictionItem& item = comres[currentTable].combs[ncomb];
				for (int k=0; k<item.e.size(); ++k)
					key[k] = record[item.e[k]];
				item.index->push(key, db[currentTable]->getNumberOfRow());
			}
			delete[] key;

			for (int c=0; c<numColumns[currentTable]; ++c)
				clusterData[currentTable][c]->push(&record[c]);
		}

		db[currentTable]->push(record);
		stringLUT[currentTable]->push(stringsToInsert);	// Insert string

		delete[] record;
	}
}

void preprocess()
{
	initialized = true;
	for (int t=0; t<T; ++t)
	{
		int *key = new int[numColumns[t]];
		for (int ncomb=0; ncomb<comres[t].combs.size(); ++ncomb)
		{
			//puts("build index");
			CompoundRestrictionItem& item = comres[t].combs[ncomb];

			for (int i=0; i<db[t]->getNumberOfRow(); ++i)
			{
				Record record = db[t]->itemAt(i);
	
				for (int cn=0; cn<item.size(); ++cn)
					key[cn] = record[item.e[cn]];
				item.index->push( key, i );
			}
		}
		delete[] key;
	}
	for (int t=0; t<T; ++t)
		for (int c=0; c<comres[t].combs.size(); ++c)
		{
			comres[t].combs[c].index->initialize();
			//puts("sort");
		}
	clusterData.resize(T);
	for (int t=0; t<T; ++t)
	{
		for (int c=0; c<numColumns[t]; ++c)
		{
			//puts("build cluster");
			clusterData[t].push_back(new Table(memory, 1));
			for (int i=0; i<db[t]->getNumberOfRow(); ++i)
			{
				Record record = db[t]->itemAt(i);
				int tmp = record[c];
				clusterData[t][c]->push(&tmp);
			}
		}
	}
}

void execute(const string& sql)
{
	static int cnt = 0;
	free(resultItems);						// Clear last query's result
	resultTables.clear();
	currentRow = 0;

	vector<string> tokens;
	chrisTokenize(sql, tokens);
	
	if(tokens[0]=="INSERT")						// INSERT
	{
		parseInsert(tokens);
	}
	else								// SELECT
	{
		vector<int> fromTableID;
		vector<ColumnRestriction> restriction;
		vector<pair<GlobalColumnID, GlobalColumnID> > equalPairs;
		
		bool emptyResult = parseSelect(tokens, selectedColumns, fromTableID,
					restriction, equalPairs, stringRestriction);

		/// If one range can never be met, return an empty set
		if (emptyResult)					
		{
			return;
		}

		/// Selected items in each table, NULL=all
		vector< vector<int>* > items(T);

		for(int i=0; i<T; ++i)
		{
			vector<int> left;
			vector<int> right;
			vector<int> columnID;
			vector<pair<int,int> > rbyC(numColumns[i]);

			bool bf = 0;
			vector<ColumnRestriction> currentRes;
			for (int j=0; j<restriction.size(); ++j)
				if (restriction[j].ID.tableID==i)
				{
					currentRes.push_back(restriction[j]);
					rbyC[restriction[j].ID.columnID] = restriction[j].range;
				}

			if (currentRes.empty()) continue;

			// Index
			CompoundRestrictionItem item = comres[i].optimumItem(currentRes);

			for (int k=0; k<item.e.size(); ++k)
			{
				left.push_back( rbyC[item.e[k]].first );
				right.push_back( rbyC[item.e[k]].second );
			}

			vector<int>* old_items = items[i];
			items[i] = select_Indexed(left, right, item.index);

			if (old_items!=NULL)
				delete old_items;

			sort(items[i]->begin(), items[i]->end());

			// Brute force
			left.clear();
			right.clear();

			for (int k=0; k<currentRes.size(); ++k)
				if (!item.covered(currentRes[k].ID.columnID))
				{
					left.push_back( currentRes[k].range.first );
					right.push_back( currentRes[k].range.second );
					columnID.push_back( currentRes[k].ID.columnID );
				}

			if (!left.empty())
			{
				vector<int>* old_items = items[i];
				items[i] = select_BruteForce(i,
						columnID,
						left,
						right,
						items[i]);

				if (old_items!=NULL)
					delete old_items;
			}
		}

		if ( fromTableID.size() == 1 )			// No join
		{
			int t = fromTableID[0];
			resultItems = vector<vector<int>* >(1, items[t]);
			resultTables.push_back(t);
			return;
		}

		/// Join
		vector<int> father(T);
		vector<int> inFrom(T);
		vector< vector<int> > tempTable(T);
		/// First dimension is which temp result
		/// Second dimension is from which table
		/// Third dimension is whichs lines of that table
		vector< vector< vector<int>* > > tempResult(T);


		for (int i=0; i<fromTableID.size(); ++i)
			inFrom[ fromTableID[i] ] = 1;

		for (int t=0; t<T; ++t)
		{
			father[t] = -1;
			tempTable[t].push_back(t);
			tempResult[t] = vector<vector<int>*>(1, items[t] ) ;
		}

		while (!equalPairs.empty())
		{
			/// Select smallest pair
			int smallest=INF;
			int si;
			for (int i=0; i<equalPairs.size(); ++i)
			{
				int rt1 = root( equalPairs[i].first.tableID, father );
				int rt2 = root( equalPairs[i].second.tableID, father );

				int current = resultRows(rt1, tempResult[rt1])
					    + resultRows(rt2, tempResult[rt2]);
				
				if (current<smallest)
				{
					smallest = current;
					si = i;
				}
			}

			int t1 = equalPairs[si].first.tableID;
			int t2 = equalPairs[si].second.tableID;

			int rt1 = root(t1, father);
			int rt2 = root(t2, father);

			/// Always big join small
			if ( resultRows(rt1, tempResult[rt1]) 
			   < resultRows(rt2, tempResult[rt2]) )
			{
				swap(t1, t2);
				swap(rt1, rt2);
				swap(equalPairs[si].first, equalPairs[si].second);
			}

			vector<vector<int>*> rtRrt1 = tempResult[rt1];

								// tempResult[rt1] <- join(t1, t2)
			tempResult[rt1] = join( t1, equalPairs[si].first.columnID,
				     t2, equalPairs[si].second.columnID,
				     tempResult[rt1], tempTable[rt1] ,
				     tempResult[rt2], tempTable[rt2] ,
				     clusterData[t1][equalPairs[si].first.columnID],
				     clusterData[t2][equalPairs[si].second.columnID]
				     );

			free(tempResult[rt2]);			             
			free(rtRrt1);
								// tempTable[rt1] <- join(t1, t2)
			for (int j=0; j<tempTable[rt2].size(); ++j)
				tempTable[rt1].push_back( tempTable[rt2][j] );
	
			tempTable[rt2].clear();

			father[rt2] = rt1;			// update father

			equalPairs.erase( equalPairs.begin() + si );
		}

		// Join remaining tables with Cartesian product
		int tRoot = 0;
		for (tRoot=0; tRoot<T; ++tRoot)			// One root
			if (inFrom[tRoot] && father[tRoot]==-1)
				break;

		for (int t=tRoot+1; t<T; ++t)			// Another root
		{
			if (inFrom[t] && father[t]==-1)
			{
				vector<vector<int>*> rtRrt1 = tempResult[tRoot];

				tempResult[tRoot] = cartesianProduct(
						tRoot,
						t,
						tempResult[tRoot],
						tempResult[t]);

				free(tempResult[t]);
				free(rtRrt1);
				
				for (int j=0; j<tempTable[t].size(); ++j)
					tempTable[tRoot].push_back( tempTable[t][j] );

				tempTable[t].clear();

				father[t] = tRoot;
			}
		}

		resultItems = tempResult[tRoot];
		resultTables = tempTable[tRoot];		
	}
	//Printf("Execution fin. %d results.\n", resultRows(resultTables[0], resultItems));
}

/// The current record's table t is from rowOfTable[t]
vector<int> rowOfTable(1000);

int next(char *row)
{
	if (resultItems.empty())
		return 0;

	if ( !resultItems[0] ) {
		// TODO		could this procedure impact the memory efficiency
		//		could this case even happen to a large table?

		/// assert: if !resultItems[t], t==0 and resultItems.size()==1 
		// create 0~R-1
		int R = db[resultTables[0]]->getNumberOfRow();
		resultItems[0] = new vector<int>(R);
		for (int i=0; i<R; ++i) (*resultItems[0])[i] = i;

	}
	if ( currentRow == resultItems.front()->size() )
		return (0);

	for (int t=0; t<resultTables.size(); ++t)
	{
		rowOfTable[ resultTables[t] ] = (*resultItems[t]) [currentRow];
	}


	/// TODO Double-check string = constant (select) and string = string (join)
	/*for (int i=0; i<stringRestriction.size(); ++i)
	{
		StringRestriction& res = stringRestriction[i];
		if ( stringLUT[res.ID.tableID]->		// Condition not met
				getString( rowOfTable[res.ID.tableID],res.ID.columnID ) 
		!= res.value ) 
		{
			currentRow++;				// Ignore this row
			return next(row);			// return next candidate
		}
	}*/

	int printCur = 0;
	for (int s=0; s<selectedColumns.size(); ++s) {
		GlobalColumnID ID = selectedColumns[s];
		
		if ( columnIsString[ID.tableID][ID.columnID] )	// STRING
		{
			writeChar(row, printCur, '\'');
			
			stringLUT[ID.tableID]->getString(
					row + printCur,
					rowOfTable[ID.tableID],
					ID.columnID );

			while ( row[printCur] != '\0' )
				++printCur;

			writeChar(row, printCur, '\'');
		}
		else						// INTEGER
		{
			writeInteger(row, printCur,
					db[ID.tableID]->itemAt( 
						rowOfTable[ID.tableID], ID.columnID ) );
		}

		// Add ','
		if (s+1 < selectedColumns.size())
			writeChar(row, printCur, ',');
	}
	row[printCur] = '\0';
	currentRow ++;

	return (1);
}

void close()
{
	for(int i=0;i<db.size();++i)
	{
		delete db[i];
	}
}

