#ifndef __ALGORITHM_H
#define __ALGORITHM_H

#include "table.h"

class CompoundIndex; 

namespace Algorithm{

	/**
	  	Select
		====

		Select in Table[tableID] with the restriction: table.column in [left, right].

		@Param	from	Only select from those records, whose row number specified by "from". 
				If "from"=NULL, we can select from the whole table.
		@Return		A vector of all resulting row numbers
	*/

	/**
		Brute force algorithm: scan over from

		columnID.size() = left.size() = right.size()
	*/
	vector<int>* select_BruteForce( int tableID, 
					vector<int> &columnID, 
					vector<int> &left, 
					vector<int> &right, 
					vector<int>* from = NULL);

	/**
		Select with the RB tree. 
		! @ChenGE 	Assures that table.column is indexed.

		@note		left <= right must be ensured
	*/
	vector<int>* select_Indexed( vector<int> &left, 
					vector<int> &right, 
					CompoundIndex* index );

	/**
		Join
		====

		Join big and small with the restriction table1.column1 = table2.column2.
		! @RuoBing 	Assures that big is always the outer loop.

		big[i].size() = bigTables.size()
		result[i].size() = bigTables.size() + smallTables.size()
	*/
	vector<vector<int>* > join_Bucket( int tableID1, 
					int columnID1, 
					int tableID2, 
					int columnID2, 
					vector<vector<int>* > &big, vector<int> &bigTables,
					vector<vector<int>* > &small, vector<int> &smallTables,
					Table *bigData, Table *smallData
					);

	// Ensure small is not null
	vector<vector<int>* > join_Indexed( int tableID1, 
					int columnID1, 
					int tableID2, 
					int columnID2, 
					vector<vector<int>* > &big, vector<int> &bigTables,				
					vector<vector<int>* > &small, vector<int> &smallTables
					);

	vector<vector<int>* > join( int tableID1, 
					int columnID1, 
					int tableID2, 
					int columnID2, 
					vector<vector<int>* > &big, vector<int> &bigTables,				
					vector<vector<int>* > &small, vector<int> &smallTables,
					Table *bigData, Table *smallData
					);

	/*int numJoinResults(int lower1, int upper1,
			int lower2, int upper2,
			int num1, num2);*/

	/**
		big[i].size() = bigTables.size()
		result[i].size() = bigTables.size() + smallTables.size()
	*/
	vector<vector<int>* > cartesianProduct( 
			int bigTableID,
			int smallTableID,
			vector<vector<int>* > &big, 
			vector<vector<int>* > &small );


	void test();
};

#endif
