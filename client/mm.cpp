#include "mm.h"
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <memory.h>

MM::MM(const char *pageFileName, long long capacity)
{
	cache = new char[capacity];

	N = 0;
	sb = 0;
	C = capacity / PageSize;
	levelCapacity = C / NumQueues;
	LRUEntry.resize(C);
	LRULevel.resize(C);
	accessCount.resize(C);
	for (int i=0; i<NumQueues; ++i)
		LRU.push_back(list<int>());

	for (int i=0; i<NumQueues; ++i)
		LRUSize.push_back(0);

	for (int i=0; i<C; ++i)
	{
		freePage.push(i);
		cpnToPPN.push_back(-1);
		dirty.push_back(0);
	}
	lastPage = -1;

	int len = strlen(pageFileName);
	this->pageFileName = new char[len+1];
	memcpy(this->pageFileName, pageFileName, len+1);
}

int MM::newPage()
{
	cpnLUT.push_back(-1);
	return N++;
}

void* MM::getPage(int ID)
{
	return swap_page(ID);
}

void MM::setPageDirty(int ID)
{
	dirty[ cpnLUT[ID] ] = 1;
}

void* MM::swap_page(int ppn)
{
	int cpn = cpnLUT[ppn];
	
	if (cpn==-1)		// Missing, swap
	{
		cpn = getFreePage();
		loadPage(cpn, ppn);
	}
	else if (lastPage!=cpn)
	{
				// Refresh LRU
		int level = LRULevel[cpn];
		LRU[level].erase( LRUEntry[cpn] );
		LRUEntry[cpn] = LRU[level].insert(LRU[level].end(), cpn);
	}

	if (lastPage != cpn)
	{
		int &level = LRULevel[cpn];
		lastPage = cpn;
		accessCount[cpn]++;

		// ---- Level up
		while (level+1 < NumQueues)
		{
			if ((accessCount[cpn]==LevelUpThreshold) 
			|| LRUSize[level+1]<levelCapacity )
			{
				if (LRUSize[level+1] < levelCapacity)
				{
					// Insert to next level
					LRU[level].erase( LRUEntry[cpn] );
					LRUSize[level]--;
					++level;
					LRUEntry[cpn] = LRU[level].insert(LRU[level].end(), cpn);
					LRUSize[level]++;
				}
				else
				{
					// Insert to next level, 
					// swap the tail of next level to this level
					LRU[level].erase( LRUEntry[cpn] );

					int cpnDown = LRU[level+1].front();
					int &levelDown = LRULevel[cpnDown];
					LRU[level+1].pop_front();

					++level;
					--levelDown;

					LRUEntry[cpn] = LRU[level].insert(
							LRU[level].end(), cpn);
					LRUEntry[cpnDown] = LRU[levelDown].insert(
							LRU[levelDown].end(), cpnDown);
				}
				accessCount[cpn] = 0;
			}
			else break;

			// Level up once for THRESHOLD, 
			// but can level up multiple times if upper level is empty
		}
	}

	return (void*)(cache + cpn*PageSize);
}

void MM::evict(int cpn)
{
	if (dirty[cpn])
		writeBack(cpn);

	assert(cpnLUT[cpnToPPN[cpn]] != -1);
	// cpn not in cache now
	cpnLUT[cpnToPPN[cpn]] = -1;

	if (lastPage==cpn)
		lastPage = -1;
}
	
void MM::writeBack(int cpn)
{
	int ppn = cpnToPPN[cpn];

	long long totalOffset = (long long)ppn * PageSize; 
	int fileCnt = totalOffset / FileSize;

	if ( fileCnt >= pageFile.size() )
	{
		char nameBuff[65536];
		sprintf(nameBuff, "%s%d", pageFileName, pageFile.size());
		pageFile.push_back( fopen(nameBuff, "w+b") );

		assert( fileCnt < pageFile.size() );
	}
	FILE *currentFile = pageFile[fileCnt];

	fseek(currentFile, totalOffset % FileSize, 0);
	fwrite(cache+cpn*PageSize, 1, PageSize, currentFile);
}

void MM::loadPage(int cpn, int ppn)
{
	cpnToPPN[cpn] = ppn;
	dirty[cpn] = 0;
	cpnLUT[ppn] = cpn;

	// Enter the lowest level
	LRULevel[cpn] = 0;
	LRUEntry[cpn] = LRU[0].insert(LRU[0].end(), cpn);
	LRUSize[0]++;
	accessCount[cpn] = 0;

	long long totalOffset = (long long)ppn * PageSize; 
	int fileCnt = totalOffset / FileSize;

	if ( fileCnt >= pageFile.size() )
	{
		char nameBuff[65536];
		sprintf(nameBuff, "%s%d", pageFileName, pageFile.size());
		pageFile.push_back( fopen(nameBuff, "w+b") );

		assert( fileCnt < pageFile.size() );
	}
	FILE *currentFile = pageFile[fileCnt];

	fseek(currentFile, totalOffset % FileSize, 0);
	fread(cache+cpn*PageSize, 1, PageSize, currentFile);
}

int MM::getFreePage()
{
	if (!freePage.empty())
	{
		int ret = freePage.top();
		freePage.pop();
		return ret;
	}
	// Pop the tail of the lowest level
	int cpn = LRU[0].front(); LRU[0].pop_front();	
	LRUSize[0]--;

	evict(cpn);
	return cpn;
}

void MM::showQueue(MM* mm)
{
	puts("Queues:");
	for (int i=NumQueues-1; i>=0; --i)
	{
		printf("Level %d:", i);
		for (list<int>::iterator iter = mm->LRU[i].begin(); iter!=mm->LRU[i].end(); ++iter)
			printf("\t%d", *iter);
		puts("");
	}
}

void MM::test()
{
	// ---- Multiqueue test
	MM* mm = new MM("data/chris.pageFile",PageSize * NumQueues * 2);

	int page7 = mm->newPage();	showQueue(mm);
	int page6 = mm->newPage();	showQueue(mm);
	int page5 = mm->newPage();	showQueue(mm);
	int page4 = mm->newPage();	showQueue(mm);
	int page3 = mm->newPage();	showQueue(mm);
	int page2 = mm->newPage();	showQueue(mm);
	int page1 = mm->newPage();	showQueue(mm);
	int page0 = mm->newPage();	showQueue(mm);

	mm->getPage(page0);		showQueue(mm);
	mm->getPage(page1);		showQueue(mm);
	mm->getPage(page2);		showQueue(mm);
	mm->getPage(page3);		showQueue(mm);
	mm->getPage(page4);		showQueue(mm);
	mm->getPage(page5);		showQueue(mm);
	mm->getPage(page6);		showQueue(mm);
	mm->getPage(page7);		showQueue(mm);

	mm->getPage(page6);		showQueue(mm);
	mm->getPage(page4);		showQueue(mm);
	mm->getPage(page6);		showQueue(mm);
	mm->getPage(page5);		showQueue(mm);
	mm->getPage(page6);		showQueue(mm);
	mm->getPage(page4);		showQueue(mm);
	mm->getPage(page5);		showQueue(mm);
	mm->getPage(page0);		showQueue(mm);
	mm->getPage(page5);		showQueue(mm);



	/**
	  Correctness test
	MM* mm = new MM(8192);

	int page1 = mm->newPage();
	int page2 = mm->newPage();

	int* page1Data = (int*)mm->getPage(page1);
	mm->setPageDirty(page1);
	page1Data[5] = 5;

	int* page2Data = (int*)mm->getPage(page2);
	mm->setPageDirty(page2);
	page2Data[5] = 7;

	int page3 = mm->newPage();

	page1Data = (int*)mm->getPage(page1);
	int* page3Data = (int*)mm->getPage(page3);
	mm->setPageDirty(page3);
	page3Data[5] = 9;

	cout << page1Data[5] << " " << page2Data[5] << " " << page3Data[5] << endl;

	page1Data = (int*)mm->getPage(page1);
	cout << page1Data[5] << " " << page2Data[5] << " " << page3Data[5] << endl;

	page2Data = (int*)mm->getPage(page2);
	cout << page1Data[5] << " " << page2Data[5] << " " << page3Data[5] << endl;
	*/
}

