#include "index.h"
#include <algorithm>
#include <cstdio>
#include <iostream>
#include <cmath>

Index::Index()
{

}

Index::~Index()
{

}

void Index::test()
{
	int keyLength = 4;
	CompareArray *compare = new CompareArray(keyLength);

	vector<int*> db;

	for (int iter=0; iter<1000000; ++iter)
	{
		if (iter % 16384 == 0)
		{
			printf(".");
			fflush(stdout);
		}

		int op = rand()%2;
		if ( op ==0  )
		{
//			puts("Insert");
			int *a = new int[keyLength+1];
			for (int i=0; i<keyLength+1; ++i)
				a[i] = rand()%5;

			db.push_back(a);
			push(a, a[keyLength]);
		}
		else
		{
			int *lower = new int[keyLength];
			int *upper = new int[keyLength];
			for (int i=0; i<keyLength; ++i)
				lower[i] = upper[i] = rand()%5;
			do{
				lower[keyLength-1] = rand()%5;
				upper[keyLength-1] = rand()%5;
			} while (0);

			if (lower[keyLength-1]>upper[keyLength-1])
				swap(lower[keyLength-1], upper[keyLength-1]);

			vector<int> result;
			rangeQuery(lower, upper, result);

			int checksum = 0;
			for (int i=0; i<result.size(); ++i)
				checksum += result[i];

			for (int i=0; i<db.size(); ++i)				
				if (!(*compare)(db[i], lower) &&
				    !(*compare)(upper, db[i]) )
				{
					checksum -= db[i][keyLength];
				}
			if (checksum){
				puts("Error!");
				exit(0);
			}
		}
	}
	puts("\nFinished");
}
