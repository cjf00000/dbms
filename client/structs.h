#ifndef __STRUCTS
#define __STRUCTS

#include <cstdlib>
using namespace std;

struct GlobalColumnID
{
	GlobalColumnID(int tableID=0, int columnID=0)
	{
		this->tableID = tableID;
		this->columnID = columnID;
	}
	int tableID, columnID;
};

struct ColumnRestriction
{
	ColumnRestriction(GlobalColumnID ID, pair<int, int> range)
	{
		this->ID = ID;
		this->range = range;
	}
	GlobalColumnID ID;
	pair<int, int> range;
};

struct StringRestriction
{
	StringRestriction(GlobalColumnID ID, char* value)
	{
		this->ID = ID;
		this->value = value;
	}
	GlobalColumnID ID;
	char* value;
};

#endif
