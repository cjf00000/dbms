#include "stringLUT.h"
#include "table.h"
#include <iostream>
#include <cstring>
#include <ctime>
#include "mm.h"
using namespace std;

const int hashPrime = 1e9 + 7;

StringLUT::StringLUT(MM *memory, vector<int> length)
{
	int cnt=0;
	for (int i=0; i<length.size(); ++i)
	{
		sBegin.push_back(cnt);
		rBegin.push_back(cnt/4);
		cnt += ((length[i]-1)/4+1) *4;
		sEnd.push_back(cnt);
		rEnd.push_back(cnt/4);
	}
	sLength = cnt;
	rLength = cnt/4;

	// rLength uncached rows
	table = new Table( memory, rLength );

	myBuffer = new char[cnt+1];
}

void StringLUT::push(vector<char*>& str)
{
	Record record = new int[rLength];
	for (int i=0; i<str.size(); ++i)
		toRecord(str[i], sEnd[i]-sBegin[i], record + rBegin[i]);

	table->push(record);			// Insert to table

	delete[] record;
}

void StringLUT::getString(char* str, int r, int c)
{
	toString(str, table->itemAt(r), rBegin[c], rEnd[c]);
	int len = sEnd[c] - sBegin[c];
	while (str[len]='\0') --len;
	str[len]='\0';
}

StringLUT::~StringLUT()
{
	delete table;
	delete[] myBuffer;
}

void StringLUT::toRecord(char* str, int sl, Record record)
{
	int strLen = strlen(str);
	for (int i=0; i<sl/4; ++i) 
	{
		record[i] = 0;
		for (int j=i*4; j<i*4+4; ++j)
		{
			record[i] = record[i] * 256;
			if (j<strLen)
				record[i] = record[i] + (unsigned char)(str[j]);
		}
	}
}

char buff[4];

void StringLUT::toString(char* str, Record record, int Begin, int End)
{
	int cnt = 0;
	for (int i=Begin; i<End; ++i)
	{
		int code = record[i];
		for (int j=0; j<4; ++j)
		{
			buff[j] = char(code%256);
			code = code/256;
		}
		str[cnt++] = buff[3];
		str[cnt++] = buff[2];
		str[cnt++] = buff[1];
		str[cnt++] = buff[0];
	}
}

int StringLUT::hash(char* str)
{
	int strLen = strlen(str);
	int code = 0;
	for (int i=0; i<strLen; ++i)
		code = code * hashPrime + str[i];
	return code;
}

void StringLUT::test()
{
	MM* memory = new MM("data/chris.pageFile", 163840);

	vector<int> length;
	length.push_back(3);
	length.push_back(5);

	StringLUT *t = new StringLUT(memory, length);

	char* st1 = "cjf";
	char* st2 = "shabi";
	vector<char*> rec;
	rec.push_back(st1);
	rec.push_back(st2);

	t->push(rec);

	st1 = "abc";
	st2 = "apple";
	t->push(rec);

	char* buff1 = new char[4];
	char* buff2 = new char[8];

	t->getString(buff1, 0, 0);
	t->getString(buff2, 1, 1);

	puts(buff1);
	puts(buff2);
}
