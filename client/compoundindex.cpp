#include "compoundindex.h"
#include "blocklist.h"
#include "diskarray.h"
#include <algorithm>
#include <cassert>
#include <cmath>

CompoundIndex::CompoundIndex(MM* memory, int keyLength)
{
	this->memory = memory;
	index.push_back( new DiskArray(memory, keyLength) );

	init = false;
	this->keyLength = keyLength;
}

int CompoundIndex::blockListID()
{
	for (int i=0; i<index.size(); ++i)
		if (index[i]->Type == 1)
			return i;
	// Never
	return -1;
}

void CompoundIndex::initialize()
{
	assert( index.front()->Type == 1);
	((DiskArray*)index.front())->initialize();

	init = true;
	index.push_back( new BlockList(keyLength) );
}

void CompoundIndex::push(int *key, int value)
{
//	printf("%d %d\n", key, value);
	if (!init)		// Push to disk array
	{
		index.front()->push(key, value);
		return;
	}
				// Push to block list
	index.back()->push(key, value);
	if (index.back()->N >= BlockListCapacity)
		shrink();
}

void CompoundIndex::rangeQuery(int *lower, int *upper,
			vector<int>& result)
{
	result.clear();

	vector<int> temp_result;
	for (int i=index.size()-1; i>=0; --i)
	{
		index[i]->rangeQuery(lower, upper, temp_result);
		result.insert( result.end(), temp_result.begin(), temp_result.end() );
	}
}

int CompoundIndex::usage()
{
	int sum = 0;
	for (int i=0; i<index.size(); ++i)
		sum += index[i]->usage();
	return sum;
}

bool CompoundIndex::worthMerge(SubIndex *small, SubIndex *large)
{
	return small->N * MergeScaleFactor >= large->N;
}

/// Convert a BlockList to DiskArray
void CompoundIndex::shrink()
{
	// A shrink chain
	while (index.size()>=2)
	{	
		if (worthMerge(index.back(), index[index.size()-2]))
		{
			assert( index[index.size()-2]->Type == 1 );
			((DiskArray*)index[index.size()-2])->mergeWith(index.back());
			delete index.back();
			index.pop_back();
		}	
		else break;
	}
	// Force shrink: convert BlockList --> DiskArray
	if (index.back()->Type==0 )
	{
		DiskArray *diskArray = new DiskArray(memory, keyLength);
		diskArray->initialize();
		diskArray->mergeWith(index.back());
		delete index.back();
		index.pop_back();
		index.push_back(diskArray);
	}

	index.push_back(new BlockList(keyLength));
}

CompoundIndex::~CompoundIndex()
{
	for (int i=0; i<index.size(); ++i)
		delete index[i];
}

void CompoundIndex::printChain()
{
/*	for (int i=0; i<index.size(); ++i)
	{
		vector<pair<int,int> > result;
		printf("%d(%s%x)%d\t", 
				index[i]->N, 
				index[i]->Type ? "DiskArray" : "BlockList", 
				index[i], result.size());

//		for (int i=0; i<result.size(); ++i)
//			printf("output: %d %d\n", result[i].first, result[i].second);
		
	}
	puts("");*/
}

void CompoundIndex::test()
{
	puts("Insert");
	BlockList *blockList = new BlockList(keyLength);

	for (int i=0; i<60000; ++i)
	{
		int *key = new int[keyLength];
		int value = rand()%5;
		for(int j = 0;j<keyLength;j++)
			key[j] = rand()%5;
		push(key, value);
		blockList->push(key, value);
	}

	puts("Initialize");
	initialize();

	puts("Insert2");
	for (int i=0; i<100; ++i)
	{
		printf(".");
		fflush(stdout);
		for (int j=0; j<3000; ++j)
		{
			int *key = new int[keyLength];
			int value = rand()%5;
			for(int j = 0;j<keyLength;j++)
				key[j] = rand()%5;
			push(key, value);			
			blockList->push(key, value);

			for (int k=0; k<4; ++k)
			{
				vector<int>result, resultStd;
	
				int *upper = new int[keyLength];
				int *lower = new int[keyLength];
				for (int i=0; i<keyLength; ++i)
					upper[i] = lower[i] = rand()%5;
				do{
					lower[keyLength-1] = rand()%5;
					upper[keyLength-1] = lower[keyLength-1] + rand()%5;
				} while ( abs(upper[keyLength-1]-lower[keyLength-1]) > 5 );
	
				rangeQuery(lower, upper, result);
				blockList->rangeQuery(lower, upper, resultStd);
	
				int checkSum = 0;
				for (int i=0; i<result.size(); ++i)
					checkSum += result[i];
				for (int i=0; i<resultStd.size(); ++i)
					checkSum -= resultStd[i];

				if (checkSum)
				{
					puts("Error");
					/*printf("Error:%d %d\n", lower, upper);
					for (int i=0; i<result.size(); ++i)
						printf("%d\t%d\n", result[i].first, result[i].second);
					puts("----");
					for (int i=0; i<resultStd.size(); ++i)
						printf("%d\t%d\n", resultStd[i].first, resultStd[i].second);
					printChain();*/
	
					exit(-1);
				}
			}
		}
		shrink();
		printChain();
	}
	puts("");
}
