#include "workload.h"
#include "tool/hash.h"
#include <algorithm>
#include <cstdio>
#include <cstring>

const int TOTAL_RECORDS = 1000000;
const int TOTAL_QUERYS = 1000;
const int ONCE_RECORDS = 1000;
const int SELECT_CONDITIONS = 5;
const int NUMBER_COUNT = 20;
const int NUMBER_RANGE = 1000000000;
const int SELECTED_COLUMNS = 5;

const char operators[] = "<>=";

string secretToString(int n)
{
	char readBuffer[100];
	sprintf(readBuffer, "%d", n);
	return string(readBuffer, readBuffer+strlen(readBuffer));
}

Workload::Workload()
{
	remainingRecords = TOTAL_RECORDS;
	remainingQuerys = TOTAL_QUERYS;

	for (int i=0; i<NUMBER_COUNT; ++i)
		numbers.push_back( rand()%NUMBER_RANGE );
}


void Workload::create(string table, vector<string> column, vector<string> type, vector<string> key)
{
	this->table.push_back(table);
	this->column.push_back(column);

	db.push_back( vector<vector<int> >() );
}


int Workload::generate(string &tableName, vector<string>& row)
{
	if (remainingRecords <= 0)
		return 0;

	int currentTable = rand()%table.size();

	tableName = table[currentTable];

	row.clear();
	for (int r = 0; r < ONCE_RECORDS; ++r )
	{
		string currentRow = "";
		vector<int> currentRecord;
		for (int c=0; c<column[currentTable].size(); ++c)
		{
			int number = numbers[rand()%NUMBER_COUNT];
			currentRecord.push_back(number);
			currentRow += secretToString( number );

			if ( c+1 < column[currentTable].size() )
				currentRow += ',';
		}
		db[currentTable].push_back(currentRecord);
		row.push_back(currentRow);
	}

	remainingRecords -= ONCE_RECORDS;
	return 1;
}

bool metCondition( int a, char op, int b )
{
	if (op == '<')
		return a<b;
	if (op == '>')
		return a>b;

	return a==b;
}

int Workload::getQuery(string &sql, int &ans)
{
	if (remainingQuerys <= 0)
		return 0;

	sql = "SELECT";
	int currentTable = rand()%table.size();
	vector<int> selectedColumns;

	int currentTableRows = db[currentTable].size();
	vector<bool> inResult(currentTableRows, 1);

	for (int i=0; i<SELECTED_COLUMNS; ++i)
	{
		int col = rand() % column[currentTable].size();
		selectedColumns.push_back(col);

		sql += " " + column[currentTable][col] + " ";
		if (i+1<SELECTED_COLUMNS) sql += ',';
	}

	sql += "FROM " + table[currentTable] + " ";

	sql += "WHERE";
	for (int i=0; i<SELECT_CONDITIONS; ++i)
	{
		int restrictColumn = rand()%column[currentTable].size();
		int compareWith = numbers[rand()%NUMBER_COUNT];
		char op = operators[rand()%3];

		sql += " " + column[currentTable][restrictColumn] + " " + op 
		     + " " + secretToString(compareWith) + " " ;

		for (int r=0; r<currentTableRows; ++r)
			if (!metCondition( db[currentTable][r][restrictColumn], op, compareWith ) )
				inResult[r] = false;

		if (i+1<SELECT_CONDITIONS) sql += "AND";
	}
	sql += ';';

	//puts(sql.c_str());
	ans = 0;
	//printf(">>>>>>>>>>>>>>>>>%d\n", selectedColumns.size());
	for (int r=0; r<currentTableRows; ++r)
		if (inResult[r])
		{
			string result;
			for (int i=0; i<selectedColumns.size(); ++i)
			{
				result += secretToString( db[currentTable][r][selectedColumns[i]] );
				if ( i+1 < selectedColumns.size() )
					result += ',';
			}
			ans += myhash(result.c_str());
			//puts(result.c_str());
		}

	--remainingQuerys;
	return 1;
}

