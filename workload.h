#ifndef __WORKLOAD
#define __WORKLOAD

#include <string>
#include <vector>
using namespace std;

class Workload
{
public:
	Workload();
	
	void create(string table, vector<string> column, vector<string> type, vector<string> key);

	int generate(string &tableName, vector<string>& row);

	int getQuery(string &sql, int &ans);

private:
	int remainingRecords;
	int remainingQuerys;

	vector<int> numbers;

	vector<string> table;
	vector<vector<string> > column;

	/// [Table][Row][Column]
	vector<vector<vector<int> > > db;
};

#endif

